# OOP PROJECT 

**lets make a game** 

For start up  :- 
1. Go through libgdx documentation, download and install it.
2. Create an account on gitlab.com
3. Read about game programming in general.

**Important Note** 

install libgdx  From : url  https://libgdx.badlogicgames.com/download.html 

install Android Studio : url  https://developer.android.com/studio/

To practice Java code use JAVA JDK , You need JDK for Libgdx also so download it first 
                    : url to install : https://www.oracle.com/technetwork/java/javase/downloads/index.html
                    
install git and use git to push and pull repository from gitlab link to install url :- https://git-scm.com/ 

learn Git commands From gitlab website URL :- https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html 

Box 2d Tutorials : https://www.gamedevelopment.blog/full-libgdx-game-tutorial-box2d

Animation Tutorials : https://www.gamefromscratch.com/post/2015/02/27/LibGDX-Video-Tutorial-Sprite-Animation.aspx 

Download :  Texture Packer for sprite sheets as done in background  Demo  

